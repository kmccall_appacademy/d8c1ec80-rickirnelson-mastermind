class Code
  PEGS = {
    "B" => :blue,
    "G" => :green,
    "O" => :orange,
    "R" => :red,
    "P" => :purple,
    "Y" => :yellow
  }

  attr_reader :pegs

  def self.parse(code_str)
    chars = code_str.chars.map(&:upcase)
    raise ArgumentError unless chars - PEGS.keys == []
    pegs = chars.map {|char| PEGS[char]} #didn't refer to PEGS initially
    Code.new(pegs) #or self.new(pegs) .. better than referring to class
  end

  def self.random
    pegs = []
    4.times { pegs << PEGS.values.sample}
    self.new(pegs)
  end

  def initialize(pegs)
    @pegs = pegs
  end

  #syntactic sugar method
  def [](i)
    @pegs[i]
  end

  def exact_matches(other)
    matches = 0
    #times method with i as argument is automatically index?
    4.times { |i| matches += 1 if @pegs[i] == other.pegs[i]}
    matches
  end

  def near_matches(other)
    matches = 0
    PEGS.values.each do |color|
      matches += [@pegs.count(color),other.pegs.count(color)].min
    end
    matches - exact_matches(other)
  end

  def ==(other)
    # didn't get this before
    return false unless other.class == Code
    @pegs == other.pegs
  end
end

class Game
  attr_reader :secret_code

  def initialize(code = nil)
    @secret_code = code || Code.random
  end

  def get_guess
    print ' Enter a guess (ex. "BGPR"): '
    Code.parse($stdin.gets.chomp) #if error, start arg with $stdin
  end

  def display_matches(code)
    #for RSPEC, mind the caps and wording of printed strings
    print "near matches: #{@secret_code.near_matches(code)}"
    print "exact matches: #{@secret_code.exact_matches(code)}"
  end
end
